import fileinput

TotalOccurence = 0
oldColor = None
for line in fileinput.input():
	data =  line.strip().split(",")
	if len(data) !=2:
		continue

	thisColor, Occurence = data

	if oldColor and oldColor != thisColor:
		print "{0}\t{1}".format(oldColor,TotalOccurence)

		TotalOccurence=0

	oldColor = thisColor
	TotalOccurence += int(Occurence)

if oldColor != None:
	print "{0}\t{1}".format(oldColor, TotalOccurence)