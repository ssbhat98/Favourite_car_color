#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  1 20:42:09 2018

@author: flame-alchemist
"""
import cv2
import numpy as np 
from matplotlib import pyplot as plt
import csv
sno=1

for no in range(1,5):
    car_cascade = cv2.CascadeClassifier('cars.xml')
    img = cv2.imread('car'+str(no)+'.jpg', 1)
    cv2.imshow('Original image',img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Detect cars
    cars = car_cascade.detectMultiScale(gray, 1.1, 1)
    sub_image=[]
# Draw border
    for (x, y, w, h) in cars:
        cv2.rectangle(img, (x,y), (x+w,y+h), (0,0,255), 2)
        sub_image.append(img[y:y+h, x:x+w])

# Show image
    plt.figure(figsize=(10,20))
    plt.imshow(img)

#COLOR RANGES IN HSV
    lower_blue = np.array([110,50,50])
    upper_blue = np.array([130,255,255])
    lower_red = np.array([0,70,50])
    upper_red = np.array([10,255,255])
    lower_black = np.array([0,0,0])
    upper_black = np.array([180,255,30])
    lower_white = np.array([0,0,200])
    upper_white = np.array([180,255,255])


    hsv_list=[[lower_blue,upper_blue],[lower_red,upper_red],[lower_black,upper_black],[lower_white,upper_white]]
    color_list=['BLUE','RED','BLACK','WHITE']
    no_of_cars=[0,0,0,0]
    
    for a in sub_image:
        hsv = cv2.cvtColor(a, cv2.COLOR_BGR2HSV)
        i=0
        for row in hsv_list:
    
        # This creates a mask for each color 
        # objects found in the frame.
            mask = cv2.inRange(hsv, row[0], row[1])
        
        # The bitwise and of the frame and mask is done so 
        # that only the blue coloured objects are highlighted 
        # and stored in res
            res = cv2.bitwise_and(a,a, mask= mask)
            if(cv2.countNonZero(mask)):
                no_of_cars[i]+=1
                i+=1
                print("\n")
        
        for i in range(0,4):
            print(color_list[i],"CARS =",no_of_cars[i])
            Data=[]
            for i in range(0,4):
                count=0
                while count<no_of_cars[i]:
                    count+=1
                    Data.append([sno,color_list[i],1])
                    sno+=1

    file=open('color.csv','a+')
    
    with file:
        writer=csv.writer(file)
        writer.writerows(Data)